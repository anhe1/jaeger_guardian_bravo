﻿using System;
using System.IO;
using System.Diagnostics;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using SprightlySoftAWS;
using SprightlySoftAWS.S3;
using Jaeger.Amazon.Helpers;
using Jaeger.Amazon.S3.Entities;

namespace Jaeger.Amazon.S3.Helpers
{
    public class Service : HelperLogData
    {
        private Settings configuracion;
        private int codigoField;
        private string mensajeField;

        /// <summary>
        /// contructor
        /// </summary>
        public Service()
        {
            this.configuracion = new Settings();
        }

        /// <summary>
        /// contructor
        /// </summary>
        /// <param name="configuracion">objeto: SettingsAmazonS3 con la información de la configuración.</param>
        public Service(Settings configuracion)
        {
            this.configuracion = configuracion;
        }

        public Service(string json)
        {
            this.configuracion = Settings.Json(json);
        }

        #region propiedades
        
        /// <summary>
        /// obtener o establecer la configuración para el servicio Amazon S3
        /// </summary>
        public Settings Configuracion
        {
            get
            {
                return this.configuracion;
            }
            set
            {
                this.configuracion = value;
            }
        }

        public int Codigo
        {
            get
            {
                return this.codigoField;
            }
            set
            {
                this.codigoField = value;
            }
        }

        public string Mensaje
        {
            get
            {
                return this.mensajeField;
            }
            set
            {
                this.mensajeField = value;
            }
        }

        #endregion

        public string CreateUrl(string bucketName, string keyName)
        {
            REST myRest = new REST();

            string requestUrl = "";
            requestUrl = myRest.BuildS3RequestURL(true, "s3.amazonaws.com", bucketName, keyName, "");

            if (this.configuracion.Region != "us-east-1")
            {
                requestUrl = myRest.BuildS3RequestURL(true, string.Concat("s3-", this.configuracion.Region, ".amazonaws.com"), bucketName, keyName, "");
            }
            else
            {
                requestUrl = myRest.BuildS3RequestURL(true, "s3.amazonaws.com", bucketName, keyName, "");
            }
            return requestUrl;
        }

        /// <summary>
        /// copiar objeto del bucket a localizacion especificada
        /// </summary>
        public bool Copy(string sourceBucketName, string sourceKeyName, string targetBucketName, string targetKeyName = "")
        {
            // Copying Amazon S3 Objects: http://docs.amazonwebservices.com/AmazonS3/latest/UsingCopyingObjects.html
            // PUT Object (Copy): http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectCOPY.html

            // This is a simple implementation of copying an object.  A more complete implementation would do the following:
            //  check if the destination file exists and prompt the user if they would like to overwrite it
            //  get the ACLs from the source file and apply them to the new file
            bool blnReturn = false;
            string newKeyName = targetBucketName;

            if (targetKeyName == "")
            {
                newKeyName = sourceKeyName;
            }

            if (newKeyName != "")
            {
                Dictionary<string, string> extraRequestHeaders = new Dictionary<string, string>();
                extraRequestHeaders.Add("x-amz-copy-source", "/" + sourceBucketName + "/" + sourceKeyName);
                extraRequestHeaders.Add("x-amz-metadata-directive", "COPY"); // 'use this request header to copy the metadata and extra headers

                SprightlySoftAWS.REST myRest = new REST();

                string requestUrl;
                requestUrl = myRest.BuildS3RequestURL(true, "s3.amazonaws.com", targetBucketName, newKeyName, "");

                string requestMethod = "PUT";

                extraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                string authorizationValue;
                authorizationValue = myRest.GetS3AuthorizationValue(requestUrl, requestMethod, extraRequestHeaders, this.configuracion.AccessKeyId, this.configuracion.SecretAccessKey);
                extraRequestHeaders.Add("Authorization", authorizationValue);

                bool retBool;
                retBool = myRest.MakeRequest(requestUrl, requestMethod, extraRequestHeaders, "");

                Debug.Print("");
                Debug.Print(myRest.LogData);
                Debug.Print("");

                XmlDocument xmlDoc;
                XmlNamespaceManager myXmlNamespaceManager;
                XmlNode xmlNode;
                string responseMessage = "";

                if (retBool == true)
                {
                    xmlDoc = new System.Xml.XmlDocument();
                    xmlDoc.LoadXml(myRest.ResponseString);

                    myXmlNamespaceManager = new System.Xml.XmlNamespaceManager(xmlDoc.NameTable);
                    myXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                    xmlNode = xmlDoc.SelectSingleNode("/amz:CopyObjectResult/amz:LastModified", myXmlNamespaceManager);
                    responseMessage = responseMessage + "LastModified = " + xmlNode.InnerText + "\r\n";

                    xmlNode = xmlDoc.SelectSingleNode("/amz:CopyObjectResult/amz:ETag", myXmlNamespaceManager);
                    responseMessage = responseMessage + "ETag = " + xmlNode.InnerText + "\r\n";
                    this.Codigo = 0;
                    this.Mensaje = responseMessage;
                    blnReturn = true;
                }
                else
                {
                    this.Codigo = 505;
                    this.Mensaje = FormatLogData(myRest.RequestURL, myRest.RequestMethod, myRest.RequestHeaders, myRest.ResponseStatusCode, myRest.ResponseStatusDescription, myRest.ResponseHeaders, myRest.ResponseStringFormatted, myRest.ErrorNumber, myRest.ErrorDescription);
                    blnReturn = false;
                }

            }
            return blnReturn;
        }

        public bool Exists(string keyName)
        {
            return this.Exists(keyName, this.configuracion.BucketName);
        }

        public bool Exists(string keyName, string bucketName)
        {
            // elabora: anhe 100520162157
            // proposito: verificar la existencia de un objeto almacenado en el bucket indicado

            // hacer una petición HEAD en el objeto para ver si existe

            // HEAD Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectHEAD.html

            REST myRest = new REST();

            string requestUrl;
            requestUrl = myRest.BuildS3RequestURL(true, "s3.amazonaws.com", bucketName, keyName, "");

            if (this.configuracion.Region != "us-east-1")
            {
                requestUrl = string.Concat(string.Concat("https://", "s3-", this.configuracion.Region, ".amazonaws.com"), "/", bucketName, "/", keyName);
            }
            else
            {
                requestUrl = string.Concat("https://s3.amazonaws.com", "/", bucketName, "/", keyName);
            }

            string requestMethod = "HEAD";

            Dictionary<String, String> extraRequestHeaders = new Dictionary<string, string>();
            extraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            string authorizationValue;
            authorizationValue = myRest.GetS3AuthorizationValue(requestUrl, requestMethod, extraRequestHeaders, this.configuracion.AccessKeyId, this.configuracion.SecretAccessKey);
            extraRequestHeaders.Add("Authorization", authorizationValue);

            bool retBool = myRest.MakeRequest(requestUrl, requestMethod, extraRequestHeaders, "");

            Debug.Print(retBool.ToString());
            Debug.Print(myRest.LogData);
            Debug.Print("");

            this.Codigo = myRest.ResponseStatusCode;

            if (myRest.ResponseStatusCode == 200)
            {
                this.Mensaje = "The object exists, you have permission to it.";
            }
            else if (myRest.ResponseStatusCode == 404)
            {
                this.Mensaje = "The object does not exists.";
            }
            else if (myRest.ResponseStatusCode == 403)
            {
                this.Mensaje = "Permission error. Someone else owns the object or your access keys are invalid.";
            }
            else
            {
                this.Mensaje = FormatLogData(myRest.RequestURL, myRest.RequestMethod, myRest.RequestHeaders, myRest.ResponseStatusCode, myRest.ResponseStatusDescription, myRest.ResponseHeaders, myRest.ResponseStringFormatted, myRest.ErrorNumber, myRest.ErrorDescription);
            }
            return this.Codigo == 200;
        }

        public bool Exists1(string requestUrl)
        {
            // elabora: anhe 100520162157
            // proposito: verificar la existencia de un objeto almacenado en el bucket indicado

            // hacer una petición HEAD en el objeto para ver si existe

            // HEAD Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectHEAD.html

            REST myRest = new REST();

            string requestMethod = "HEAD";

            Dictionary<String, String> extraRequestHeaders = new Dictionary<string, string>();
            extraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            string authorizationValue;
            authorizationValue = myRest.GetS3AuthorizationValue(requestUrl, requestMethod, extraRequestHeaders, this.configuracion.AccessKeyId, this.configuracion.SecretAccessKey);
            extraRequestHeaders.Add("Authorization", authorizationValue);

            bool retBool = myRest.MakeRequest(requestUrl, requestMethod, extraRequestHeaders, "");

            //Debug.Print(retBool.ToString());
            //Debug.Print(myRest.LogData);
            //Debug.Print("");

            this.Codigo = myRest.ResponseStatusCode;

            if (myRest.ResponseStatusCode == 200)
            {
                this.Mensaje = "The object exists, you have permission to it.";
            }
            else if (myRest.ResponseStatusCode == 404)
            {
                this.Mensaje = "The object does not exists.";
            }
            else if (myRest.ResponseStatusCode == 403)
            {
                this.Mensaje = "Permission error. Someone else owns the object or your access keys are invalid.";
            }
            else
            {
                this.Mensaje = FormatLogData(myRest.RequestURL, myRest.RequestMethod, myRest.RequestHeaders, myRest.ResponseStatusCode, myRest.ResponseStatusDescription, myRest.ResponseHeaders, myRest.ResponseStringFormatted, myRest.ErrorNumber, myRest.ErrorDescription);
            }
            Console.WriteLine(this.Mensaje);
            return this.Codigo == 200;
        }

        public bool Delete(string bucketName, string keyName)
        {
            // The following code deletes an object in a bucket.  If versioning on the bucket is enabled,
            // the call will replace the object with a Delete Marker.

            // DELETE Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectDELETE.html

            // Dim MyDialogResult As System.Windows.Forms.DialogResult
            // MyDialogResult = MessageBox.Show("Are you sure you would like to delete the object " & TextBoxObjectKeyName.Text & "?", "Delete Object", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

            // If MyDialogResult = System.Windows.Forms.DialogResult.Yes Then

            REST myRest = new REST();

            String requestUrl;
            requestUrl = myRest.BuildS3RequestURL(true, "s3.amazonaws.com", bucketName, keyName, "");

            string requestMethod = "DELETE";

            Dictionary<String, String> extraRequestHeaders = new Dictionary<string, string>();
            extraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            string authorizationValue;
            authorizationValue = myRest.GetS3AuthorizationValue(requestUrl, requestMethod, extraRequestHeaders, this.configuracion.AccessKeyId, this.configuracion.SecretAccessKey);
            extraRequestHeaders.Add("Authorization", authorizationValue);

            bool retBool;
            retBool = myRest.MakeRequest(requestUrl, requestMethod, extraRequestHeaders, "");

            Debug.Print("");
            Debug.Print(myRest.LogData);
            Debug.Print("");

            if (retBool == true)
            {
                this.Codigo = 0;
                this.Mensaje = "The object was deleted.";
            }
            else
            {
                this.Codigo = 505;
                this.Mensaje = FormatLogData(myRest.RequestURL, myRest.RequestMethod, myRest.RequestHeaders, myRest.ResponseStatusCode, myRest.ResponseStatusDescription, myRest.ResponseHeaders, myRest.ResponseStringFormatted, myRest.ErrorNumber, myRest.ErrorDescription);
            }

            return retBool;
        }

        public string SetObjectACLs(string bucketName, string keyName, bool myDialogResult = false)
        {
            // PUT Object acl: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectPUTacl.html

            //System.Windows.Forms.DialogResult myDialogResult;
            // myDialogResult = MessageBox.Show("Would you like everyone to have the permission to download this object?", "Set ACLs", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)
            //myDialogResult = System.Windows.Forms.DialogResult.Yes;

            REST myRest = new REST();

            String requestUrl;
            requestUrl = myRest.BuildS3RequestURL(true, "s3.amazonaws.com", bucketName, keyName, "?acl");

            String requestMethod = "GET";

            Dictionary<String, String> extraRequestHeaders = new Dictionary<string, string>();
            extraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            String authorizationValue;
            authorizationValue = myRest.GetS3AuthorizationValue(requestUrl, requestMethod, extraRequestHeaders, this.configuracion.AccessKeyId, this.configuracion.SecretAccessKey);
            extraRequestHeaders.Add("Authorization", authorizationValue);

            // get the owner of the object.  It will be used when constructing the Access Control List.
            bool retBool;
            retBool = myRest.MakeRequest(requestUrl, requestMethod, extraRequestHeaders, "");

            System.Xml.XmlDocument xmlDoc;
            System.Xml.XmlNamespaceManager myXmlNamespaceManager;
            System.Xml.XmlNode xmlNode;

            if (retBool == true)
            {
                xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.LoadXml(myRest.ResponseString);

                myXmlNamespaceManager = new System.Xml.XmlNamespaceManager(xmlDoc.NameTable);
                myXmlNamespaceManager.AddNamespace("amz", "http://s3.amazonaws.com/doc/2006-03-01/");

                xmlNode = xmlDoc.SelectSingleNode("/amz:AccessControlPolicy/amz:Owner/amz:ID", myXmlNamespaceManager);

                // construct an Access Control List.  Grant the AllUsers group READ permission.
                // Access Control Lists: http://docs.amazonwebservices.com/AmazonS3/latest/S3_ACLs.html

                String putXml = "";
                putXml = putXml + "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                putXml = putXml + "<AccessControlPolicy xmlns=\"http://s3.amazonaws.com/doc/2006-03-01/\">";
                putXml = putXml + "<Owner>";
                putXml = putXml + "<ID>" + xmlNode.InnerText + "</ID>";
                putXml = putXml + "</Owner>";
                putXml = putXml + "<AccessControlList>";
                putXml = putXml + "<Grant>";
                putXml = putXml + "<Grantee xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"CanonicalUser\">";
                putXml = putXml + "<ID>" + xmlNode.InnerText + "</ID>";
                putXml = putXml + "</Grantee>";
                putXml = putXml + "<Permission>FULL_CONTROL</Permission>";
                putXml = putXml + "</Grant>";
                if (myDialogResult == true)
                {
                    putXml = putXml + "<Grant>";
                    putXml = putXml + "<Grantee xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"Group\">";
                    putXml = putXml + "<URI>http://acs.amazonaws.com/groups/global/AllUsers</URI>";
                    putXml = putXml + "</Grantee>";
                    putXml = putXml + "<Permission>READ</Permission>";
                    putXml = putXml + "</Grant>";
                }
                putXml = putXml + "</AccessControlList>";
                putXml = putXml + "</AccessControlPolicy>";

                requestMethod = "PUT";

                requestUrl = myRest.BuildS3RequestURL(true, "s3.amazonaws.com", bucketName, keyName, "?acl");

                extraRequestHeaders = new Dictionary<String, String>();
                extraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));
                authorizationValue = myRest.GetS3AuthorizationValue(requestUrl, requestMethod, extraRequestHeaders, this.configuracion.AccessKeyId, this.configuracion.SecretAccessKey);
                extraRequestHeaders.Add("Authorization", authorizationValue);

                retBool = myRest.MakeRequest(requestUrl, requestMethod, extraRequestHeaders, putXml);

                Debug.Print("");
                Debug.Print(myRest.LogData);
                Debug.Print("");

                if (retBool == true)
                {
                    this.Codigo = 0;
                    this.Mensaje = "ACLs were set on the object.";
                }
                else
                {
                    this.Codigo = 505;
                    this.Mensaje = FormatLogData(myRest.RequestURL, myRest.RequestMethod, myRest.RequestHeaders, myRest.ResponseStatusCode, myRest.ResponseStatusDescription, myRest.ResponseHeaders, myRest.ResponseStringFormatted, myRest.ErrorNumber, myRest.ErrorDescription);
                    Debug.Print(this.Mensaje);
                }
            }
            else
            {
                this.Codigo = 505;
                this.Mensaje = FormatLogData(myRest.RequestURL, myRest.RequestMethod, myRest.RequestHeaders, myRest.ResponseStatusCode, myRest.ResponseStatusDescription, myRest.ResponseHeaders, myRest.ResponseStringFormatted, myRest.ErrorNumber, myRest.ErrorDescription);
                Debug.Print(this.Mensaje);
            }
            return requestUrl;
        }

        public string Upload(string uploadFileName, string keyname, string uploadContentType, bool uploadMakePublic, bool testing)
        {
            return this.Upload(testing, uploadFileName, keyname, this.configuracion.BucketName, this.configuracion.Folder, uploadContentType, uploadMakePublic);
        }

        public string Upload(bool testing, string fileName, string keyname, string bucketName = "", string folderName = "", string contentType = "", bool makePublic = false, bool usessse = false, string storageClass = "STANDARD")
        {
            if (File.Exists(fileName))
            {
                //PUT Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectPUT.html
                Dictionary<string, string> extraRequestHeaders = new Dictionary<string, string>();

                if (contentType != "")
                {
                    extraRequestHeaders.Add("Content-Type", contentType);
                }

                if (makePublic)
                {
                    //add a x-amz-acl header with the value of public-read to make the uploaded file public
                    //PUT Object acl: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectPUTacl.html
                    extraRequestHeaders.Add("x-amz-acl", "public-read");
                }

                if (usessse)
                {
                    //Specifying Server-Side Encryption Using REST API: http://docs.amazonwebservices.com/AmazonS3/latest/dev/SSEUsingRESTAPI.html
                    extraRequestHeaders.Add("x-amz-server-side-encryption", "AES256");
                }

                if (storageClass == "REDUCED_REDUNDANCY")
                {
                    //add a x-amz-storage-class header with the value of REDUCED_REDUNDANCY to make the uploaded file reduced redundancy
                    extraRequestHeaders.Add("x-amz-storage-class", "REDUCED_REDUNDANCY");
                }

                //add a Content-MD5 header to ensure data is not corrupted over the network
                //Amazon will return an error if the MD5 they calulate does not match the MD5 you send
                CalculateHash myCalculateHash = new CalculateHash();

                string myMD5;
                myMD5 = myCalculateHash.CalculateMD5FromFile(fileName);
                extraRequestHeaders.Add("Content-MD5", myMD5);

                SprightlySoftAWS.S3.Upload myUpload = new SprightlySoftAWS.S3.Upload();

                string requestUrl;

                if (testing == false)
                {
                    folderName = "sandbox";
                }

                if (folderName != "")
                {
                    bucketName = String.Concat(bucketName, "/", folderName);
                }

                if (this.configuracion.Region != "us-east-1")
                {
                    requestUrl = string.Concat(string.Concat("https://", "s3-", this.configuracion.Region, ".amazonaws.com"), "/", bucketName, "/", keyname);
                }
                else
                {
                    requestUrl = string.Concat("https://s3.amazonaws.com", "/", bucketName, "/", keyname);
                }

                string requestMethod = "PUT";

                extraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                string authorizationValue;
                authorizationValue = myUpload.GetS3AuthorizationValue(requestUrl, requestMethod, extraRequestHeaders, this.configuracion.AccessKeyId, this.configuracion.SecretAccessKey);
                extraRequestHeaders.Add("Authorization", authorizationValue);

                bool retBool;
                retBool = myUpload.UploadFile(requestUrl, requestMethod, extraRequestHeaders, fileName);

                Debug.Print("");
                Debug.Print(myUpload.LogData);
                Debug.Print("");

                if (retBool == true)
                {
                    this.Codigo = 0;
                    this.Mensaje = "Upload complete.";
                    return requestUrl;
                }
                else
                {
                    this.Mensaje = FormatLogData(myUpload.RequestURL, myUpload.RequestMethod, myUpload.RequestHeaders, myUpload.ResponseStatusCode, myUpload.ResponseStatusDescription, myUpload.ResponseHeaders, myUpload.ResponseStringFormatted, myUpload.ErrorNumber, myUpload.ErrorDescription);
                    this.Codigo = 505;
                }
            }
            else
            {
                this.Codigo = 2;
                this.Mensaje = "No se tiene acceso al archivo";
            }
            return string.Empty;
        }

        public bool UploadFile(string uploadFileName, string bucketName, string keyname, string uploadContentType = "", bool uploadMakePublic = false, bool uploadUseSse = false, string uploadStorageClass = "STANDARD")
        {
            bool blnReturn = false;
            if (File.Exists(uploadFileName))
            {
                string strUploadKeyName = keyname; //Path.GetFileName(uploadFileName)
                // PUT Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectPUT.html
                Dictionary<string, string> extraRequestHeaders = new Dictionary<string, string>();

                if (uploadContentType != "")
                {
                    extraRequestHeaders.Add("Content-Type", uploadContentType);
                }

                if (uploadMakePublic == true)
                {
                    // add a x-amz-acl header with the value of public-read to make the uploaded file public
                    // PUT Object acl: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectPUTacl.html
                    extraRequestHeaders.Add("x-amz-acl", "public-read");
                }

                if (uploadUseSse)
                {
                    // Specifying Server-Side Encryption Using REST API: http://docs.amazonwebservices.com/AmazonS3/latest/dev/SSEUsingRESTAPI.html
                    extraRequestHeaders.Add("x-amz-server-side-encryption", "AES256");
                }

                if (uploadStorageClass == "REDUCED_REDUNDANCY")
                {
                    //add a x-amz-storage-class header with the value of REDUCED_REDUNDANCY to make the uploaded file reduced redundancy
                    extraRequestHeaders.Add("x-amz-storage-class", "REDUCED_REDUNDANCY");
                }

                // add a Content-MD5 header to ensure data is not corrupted over the network
                // Amazon will return an error if the MD5 they calulate does not match the MD5 you send
                CalculateHash myCalculateHash = new CalculateHash();

                string myMD5;
                myMD5 = myCalculateHash.CalculateMD5FromFile(uploadFileName);
                extraRequestHeaders.Add("Content-MD5", myMD5);

                Upload myUpload = new Upload();

                string requestUrl;
                requestUrl = myUpload.BuildS3RequestURL(true, "s3.amazonaws.com", bucketName, strUploadKeyName, "");
                string requestMethod = "PUT";

                extraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                String authorizationValue;
                authorizationValue = myUpload.GetS3AuthorizationValue(requestUrl, requestMethod, extraRequestHeaders, this.configuracion.AccessKeyId, this.configuracion.SecretAccessKey);
                extraRequestHeaders.Add("Authorization", authorizationValue);

                bool retBool;
                retBool = myUpload.UploadFile(requestUrl, requestMethod, extraRequestHeaders, uploadFileName);

                Debug.Print("");
                Debug.Print(myUpload.LogData);
                Debug.Print("");

                if (retBool == true)
                {
                    this.Mensaje = "Upload complete.";
                    this.Codigo = 0;
                    blnReturn = true;
                }
                else
                {
                    this.Mensaje = FormatLogData(myUpload.RequestURL, myUpload.RequestMethod, myUpload.RequestHeaders, myUpload.ResponseStatusCode, myUpload.ResponseStatusDescription, myUpload.ResponseHeaders, myUpload.ResponseStringFormatted, myUpload.ErrorNumber, myUpload.ErrorDescription);
                    this.Codigo = 505;
                }
            }
            else
            {
                this.Mensaje = "No se tiene acceso al archivo";
                this.Codigo = 505;
            }
            return blnReturn;
        }

        public bool Download(string downloadFileName, string downloadKeyName, string bucketName)
        {
            bool blnResult = false;
            if (Directory.Exists(Path.GetDirectoryName(downloadFileName)))
            {
                // GET Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectGET.html
                Download myDownload = new Download();
                string requestUrl;

                requestUrl = string.Concat("https://s3.amazonaws.com/", bucketName, "/", downloadKeyName);
                string requestMethod = "GET";

                Dictionary<string, string> extraRequestHeaders = new Dictionary<string, string>();
                extraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

                string authorizationValue;
                authorizationValue = myDownload.GetS3AuthorizationValue(requestUrl, requestMethod, extraRequestHeaders, this.configuracion.AccessKeyId, this.configuracion.SecretAccessKey);
                extraRequestHeaders.Add("Authorization", authorizationValue);

                bool retBool;
                retBool = myDownload.DownloadFile(requestUrl, requestMethod, extraRequestHeaders, downloadFileName, false);

                Debug.Print("");
                Debug.Print(myDownload.LogData);
                Debug.Print("");

                if (retBool == true)
                {
                    this.Mensaje = "Download complete.";
                    this.Codigo = 0;
                    blnResult = true;
                }
                else
                {
                    this.Mensaje = FormatLogData(myDownload.RequestURL, myDownload.RequestMethod, myDownload.RequestHeaders, myDownload.ResponseStatusCode, myDownload.ResponseStatusDescription, myDownload.ResponseHeaders, myDownload.ResponseStringFormatted, myDownload.ErrorNumber, myDownload.ErrorDescription);
                    this.Codigo = 505;
                }
            }
            else
            {
                this.Mensaje = "The local file path does not exist.";
                this.Codigo = 505;
            }
            return blnResult;
        }

        public bool Download(string downloadKeyName, string bucketName, ref Stream result)
        {
            // GET Object: http://docs.amazonwebservices.com/AmazonS3/latest/API/RESTObjectGET.html
            SprightlySoftAWS.S3.Download myDownload = new Download();
            string requestUrl;

            requestUrl = myDownload.BuildS3RequestURL(true, "s3.amazonaws.com", bucketName, downloadKeyName, "");

            string requestMethod = "GET";

            Dictionary<string, string> extraRequestHeaders = new Dictionary<string, string>();
            extraRequestHeaders.Add("x-amz-date", DateTime.UtcNow.ToString("r"));

            string authorizationValue;

            try
            {
                authorizationValue = myDownload.GetS3AuthorizationValue(requestUrl, requestMethod, extraRequestHeaders, this.configuracion.AccessKeyId, this.configuracion.SecretAccessKey);
            }
            catch (Exception)
            {
                return false;
            }

            extraRequestHeaders.Add("Authorization", authorizationValue);

            bool retBool = false;
            // este metodo no esta definido en la clase de AWS
            //retBool = myDownload.DownloadFileToStream(requestUrl, requestMethod, extraRequestHeaders, result);
            this.Mensaje = "Esta función no esta disponible.";

            Debug.Print("");
            Debug.Print(myDownload.LogData);
            Debug.Print("");

            if (retBool == true)
            {
                this.Codigo = 0;
                this.Mensaje = "Download complete.";
                return retBool;
            }
            else
            {
                this.Mensaje = FormatLogData(myDownload.RequestURL, myDownload.RequestMethod, myDownload.RequestHeaders, myDownload.ResponseStatusCode, myDownload.ResponseStatusDescription, myDownload.ResponseHeaders, myDownload.ResponseStringFormatted, myDownload.ErrorNumber, myDownload.ErrorDescription);
                this.Codigo = 505;
                return false;
            }
        }

        
    }
}
