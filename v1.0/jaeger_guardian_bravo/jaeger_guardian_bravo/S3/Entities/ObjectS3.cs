﻿using System;
using System.Linq;

namespace Jaeger.Amazon.S3.Entities
{
    public class ObjectS3
    {
        private int fileSizeField;
        private bool publicField;
        private bool completedField;
        private string bucketNameField;
        private string keyNameField;
        private string fileNameField;
        private string fileExtField;
        private string contentTypeField;
        private string descriptionField;
        private string urlField;

        public int FileSize
        {
            get
            {
                return this.fileSizeField;
            }
            set
            {
                this.fileSizeField = value;
            }
        }

        public bool IsPublic
        {
            get
            {
                return this.publicField;
            }
            set
            {
                this.publicField = value;
            }
        }

        public bool Completed
        {
            get
            {
                return this.completedField;
            }
            set
            {
                this.completedField = value;
            }
        }

        public string BucketName
        {
            get
            {
                return this.bucketNameField;
            }
            set
            {
                this.bucketNameField = value;
            }
        }

        public string KeyName
        {
            get
            {
                return this.keyNameField;
            }
            set
            {
                this.keyNameField = value;
            }
        }

        public string FileName
        {
            get
            {
                return this.fileNameField;
            }
            set
            {
                this.fileNameField = value;
            }
        }

        public string FileExt
        {
            get
            {
                return this.fileExtField;
            }
            set
            {
                this.fileExtField = value;
            }
        }

        public string ContentType
        {
            get
            {
                return this.contentTypeField;
            }
            set
            {
                this.contentTypeField = value;
            }
        }

        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        public string URL
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
            }
        }
    }
}
