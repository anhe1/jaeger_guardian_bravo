﻿using System;
using System.Linq;

namespace Jaeger.Amazon.S3.Entities
{
    public class ItemObject
    {

        private bool internalIsBucket;
        private bool internalIsFolder;
        private string internalBucketName;
        private string internalKeyName;
        private string internalDisplayName;
        private long internalSize;
        private DateTime internalDate;
        private string internalFileType;
        private string internalETag;
        private string internalOwnerID;
        private string internalOwnerName;
        private string internalSortName;
        private DateTime internalSortDate;
        private string internalSortFileType;

        public bool IsBucket
        {
            get
            {
                return internalIsBucket;
            }
            set
            {
                internalIsBucket = value;
            }
        }

        public bool IsFolder
        {
            get
            {
                return internalIsFolder;
            }
            set
            {
                internalIsFolder = value;
            }
        }

        public string BucketName
        {
            get
            {
                return internalBucketName;
            }
            set
            {
                internalBucketName = value;
            }
        }

        public string KeyName
        {
            get
            {
                return internalKeyName;
            }
            set
            {
                internalKeyName = value;
            }
        }

        public string DisplayName
        {
            get
            {
                return internalDisplayName;
            }
            set
            {
                internalDisplayName = value;
            }
        }

        public long Size
        {
            get
            {
                return internalSize;
            }
            set
            {
                internalSize = value;
            }
        }

        public DateTime Date
        {
            get
            {
                return internalDate;
            }
            set
            {
                internalDate = value;
            }
        }

        public string FileType
        {
            get
            {
                return internalFileType;
            }
            set
            {
                internalFileType = value;
            }
        }

        public string ETag
        {
            get
            {
                return internalETag;
            }
            set
            {
                internalETag = value;
            }
        }

        public string OwnerID
        {
            get
            {
                return internalOwnerID;
            }
            set
            {
                internalOwnerID = value;
            }
        }

        public string OwnerName
        {
            get
            {
                return internalOwnerName;
            }
            set
            {
                internalOwnerName = value;
            }
        }

        public string SortName
        {
            get
            {
                return internalSortName;
            }
            set
            {
                internalSortName = value;
            }
        }

        public DateTime SortDate
        {
            get
            {
                return internalSortDate;
            }
            set
            {
                internalSortDate = value;
            }
        }

        public string SortFileType
        {
            get
            {
                return internalSortFileType;
            }
            set
            {
                internalSortFileType = value;
            }
        }
    }
}
