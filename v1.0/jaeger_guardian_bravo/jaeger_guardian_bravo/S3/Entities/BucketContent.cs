﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Jaeger.Amazon.S3.Enums;

namespace Jaeger.Amazon.S3.Entities
{
    [JsonObject]
    public class BucketContent
    {
        /// <summary>
        /// contructor
        /// </summary>
        public BucketContent()
        {

        }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="json"></param>
        public BucketContent(string json)
        {
            try
            {
                BucketContent conf = JsonConvert.DeserializeObject<BucketContent>(json);
                if (conf != null)
                {
                    this.Key = conf.Key;
                    this.KeyName = conf.KeyName;
                    this.MakePublic = conf.MakePublic;
                    this.StorageClass = conf.StorageClass;
                    this.ContentType = conf.ContentType;
                    this.ETag = conf.ETag;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [JsonProperty("key")]
        public string Key { get; set; }
        
        [JsonProperty("keyName")]
        public string KeyName { get; set; }
        
        [JsonProperty("contentType")]
        public string ContentType { get; set; }
        
        [JsonProperty("eTag")]
        public string ETag { get; set; }
        
        [JsonProperty("makePublic")]
        public bool MakePublic { get; set; }
        
        [JsonProperty("key")]
        public EnumStorageClass StorageClass { get; set; }

        public string ToJson(Formatting objFormat = 0)
        {
            return JsonConvert.SerializeObject(this, objFormat);
        }

        public static BucketContent Json(string json)
        {
            try
            {
                return JsonConvert.DeserializeObject<BucketContent>(json);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
    }
}
