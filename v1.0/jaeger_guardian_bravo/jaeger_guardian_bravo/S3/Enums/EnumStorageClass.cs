﻿using System;
using System.Linq;

namespace Jaeger.Amazon.S3.Enums
{
    public enum EnumStorageClass
    {
        Standar,
        ReducedRedundancy
    }
}
