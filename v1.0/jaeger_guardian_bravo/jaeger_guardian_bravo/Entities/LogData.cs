﻿using System;
using System.Linq;

namespace Jaeger.Amazon.Entities
{
    public class LogData
    {
        public string ErrorDescription { get; set; }
        public string ErrorNumber { get; set; }
        public string RequestURL { get; set; }
        public string RequestMethod { get; set; }
        public string RequestHeader { get; set; }
    }
}
