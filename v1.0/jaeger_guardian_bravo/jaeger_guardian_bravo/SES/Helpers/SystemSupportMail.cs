﻿using Jaeger.Amazon.SES.Abstract;

namespace Jaeger.Amazon.SES.Helpers
{
    public class SystemSupportMail : MasterMailServer
    {
        public SystemSupportMail()
        {
            this.senderMail = "";
            this.password = "";
            this.host = "";
            this.port = 587;
            this.ssl = true;
            this.initializeSmtpClient();
        }
    }
}
