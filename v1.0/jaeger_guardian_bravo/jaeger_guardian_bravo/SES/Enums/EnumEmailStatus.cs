﻿using System;
using System.Linq;

namespace Jaeger.Amazon.SES.Enums
{
    public enum EnumEmailStatus
    {
        OnHold,
        Read,
        Unread,
        Ready,
        Enviado,
        Error
    }
}
