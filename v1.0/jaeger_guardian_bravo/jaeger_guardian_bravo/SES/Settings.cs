﻿/// develop:
/// purpose: configuracion para el servicio SES de Amazon (Simple Email Service)
using System;
using System.ComponentModel;
using Newtonsoft.Json;
using Jaeger.Amazon.Entities;

namespace Jaeger.Amazon.SES
{
    [JsonObject]
    [TypeConverter(typeof(PropertiesConvert))]
    public class Settings : PropertyChangeImplementation
    {
        private bool enabledField;
        private string usuarioField;
        private string passwordField;
        private string servidorField;
        private string puertoField = "446";
        private string correoField;
        private bool sslField = true;

        public Settings()
        {

        }

        public Settings(string json)
        {
            try
            {
                Settings conf = JsonConvert.DeserializeObject<Settings>(json);
                if (conf != null)
                {
                    this.enabledField = conf.Enabled;
                    this.usuarioField = conf.Usuario;
                    this.passwordField = conf.Password;
                    this.servidorField = conf.Servidor;
                    this.puertoField = conf.Puerto;
                    this.correoField = conf.Email;
                    this.sslField = conf.SSL;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("Correo")]
        [JsonProperty("email")]
        public string Email
        {
            get
            {
                return this.correoField;
            }
            set
            {
                this.correoField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("Habilitado")]
        [JsonProperty("enabled")]
        public bool Enabled
        {
            get
            {
                return this.enabledField;
            }
            set
            {
                this.enabledField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("Host")]
        [JsonProperty("host")]
        public string Servidor
        {
            get
            {
                return this.servidorField;
            }
            set
            {
                this.servidorField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("Contraseña")]
        [JsonProperty("pass")]
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("Port")]
        [JsonProperty("port")]
        public string Puerto
        {
            get
            {
                return this.puertoField;
            }
            set
            {
                this.puertoField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("SSL")]
        [JsonProperty("ssl")]
        public bool SSL
        {
            get
            {
                return this.sslField;
            }
            set
            {
                this.sslField = value;
                this.OnPropertyChanged();
            }
        }

        [Category("SES")]
        [Description("")]
        [DisplayName("Usuario")]
        [JsonProperty("user")]
        public string Usuario
        {
            get
            {
                return this.usuarioField;
            }
            set
            {
                this.usuarioField = value;
                this.OnPropertyChanged();
            }
        }
    }
}