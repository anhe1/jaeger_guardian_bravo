using System.Xml.Serialization;
using Jaeger.Amazon.Entities;

namespace Jaeger.Amazon.SES.Entities
{
    public class EmailAttachment : PropertyChangeImplementation
    {
        private int privateId;
        private bool privateSelected;
        private string privateNameFile;
        private string privateContentType;
        private string privateContentB64;

        public EmailAttachment()
        {
            this.privateSelected = true;
            this.privateNameFile = string.Empty;
            this.privateContentType = "application/octet-stream";
            this.privateContentB64 = string.Empty;
        }

        [XmlAttribute]
        public string ContentB64
        {
            get
            {
                return this.privateContentB64;
            }
            set
            {
                this.privateContentB64 = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string ContentType
        {
            get
            {
                return this.privateContentType;
            }
            set
            {
                this.privateContentType = value;
                this.OnPropertyChanged();
            }
        }

        [XmlIgnore]
        public int Id
        {
            get
            {
                return this.privateId;
            }
            set
            {
                this.privateId = value;
                this.OnPropertyChanged();
            }
        }

        [XmlAttribute]
        public string NombreArchivo
        {
            get
            {
                return this.privateNameFile;
            }
            set
            {
                this.privateNameFile = value;
                this.OnPropertyChanged();
            }
        }

        [XmlIgnore]
        public bool Seleccionado
        {
            get
            {
                return this.privateSelected;
            }
            set
            {
                this.privateSelected = value;
                this.OnPropertyChanged();
            }
        }
    }
}