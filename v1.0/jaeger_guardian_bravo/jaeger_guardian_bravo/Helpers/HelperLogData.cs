﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace Jaeger.Amazon.Helpers
{
    public class HelperLogData
    {
        public static string FormatLogData(string requestUrl, string requestMethod, Dictionary<string, string> requestHeaders, int responseStatusCode, string responseStatusDescription, Dictionary<string, string> responseHeaders, string responseString, int errorNumber, string errorDescription)
        {
            string returnString = "";

            if (errorNumber != 0)
            {
                returnString += "SprightlySoft ErrorDescription: " + errorDescription + Environment.NewLine;
                returnString += "SprightlySoft ErrorNumber: " + errorNumber + Environment.NewLine;
                returnString += Environment.NewLine;
            }

            if (responseStatusCode != 0)
            {
                returnString += "Request URL: " + requestUrl + Environment.NewLine;
                returnString += "Request Method: " + requestMethod + Environment.NewLine;

                foreach (KeyValuePair<string, string> MyHeader in requestHeaders)
                {
                    returnString += "Request Header: " + MyHeader.Key + ":" + MyHeader.Value + Environment.NewLine;
                }

                returnString += Environment.NewLine;
                returnString += "Response Status Code: " + responseStatusCode + Environment.NewLine;
                returnString += "Response Status Description: " + responseStatusDescription + Environment.NewLine;

                foreach (KeyValuePair<string, string> MyHeader in responseHeaders)
                {
                    returnString += "Response Header: " + MyHeader.Key + ":" + MyHeader.Value + Environment.NewLine;
                }

                if (responseString != "")
                {
                    returnString += Environment.NewLine;

                    try
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(responseString);

                        returnString += "Response XML: " + Environment.NewLine + responseString + Environment.NewLine;

                        System.Xml.XmlNode xmlNode;
                        xmlNode = xmlDoc.SelectSingleNode("/Error/Message");

                        if (xmlNode != null)
                        {
                            returnString += Environment.NewLine;
                            returnString += "Amazon Error Message: " + xmlNode.InnerText + Environment.NewLine;
                        }
                    }
                    catch (Exception e)
                    {
                        returnString += "Response String: " + Environment.NewLine + responseString + Environment.NewLine;
                    }

                }
            }

            return returnString;
        }
    }
}
